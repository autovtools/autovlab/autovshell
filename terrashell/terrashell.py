"""TerraShell: A python wrapper around the 'terrashell' program.

SECURITY WARNING:
    (Likely) Insecure against malicious input.
    Threat Model: only trusted admins can call functions
"""

import json
import shlex
import shutil
import logging
import traceback

from gevent import subprocess
import coloredlogs

from terrashell import utils
from terrashell.hvcmd import (
    HvCmd, GetUsedVLANs, GetVLAN, GetVmTags, GetVmByTags,
    GetMacAddresses, GetVm, RebootVM
)
from terrashell.guestcmd import (
    GuestCmd, GuestExec, GuestRunScript, GuestUpload, GuestDownload, GuestCustomize
)

class TerraShell():
    """
    TerraShell: A python wrapper around the 'terrashell' program.
    """

    def __init__(self, log_level=logging.INFO, log_file=None,
                    show_line_info=False, vsphere_creds=None
    ):
        self.logger = utils.get_logger(
            self.__class__.__name__,
            level=log_level,
            log_file=log_file,
            show_line_info=show_line_info
        )
        self.terrashell_path = shutil.which('terrashell')
        self.vsphere_creds = vsphere_creds

        if self.terrashell_path is None:
            msg = "terrashell not found in $PATH"
            self.logger.critical(msg)
            raise Exception(msg)
        else:
           self.terrashell_path = [self.terrashell_path, "-q"]

    def run(self, cmd, silent=False, set_returncode=True):
        """
        Invoke terrashell, piping the desired command to stdin.
        
        Return value is True on success and False otherwise
        """
        self.logger.info(cmd)
        if type(cmd) is str:
            cmd = cmd.encode()
        if silent:
            # Suppress any output; useful for 'exists' checks that safely fail
            cmd += b" 2>&1 | Out-Null;"
        if set_returncode: 
            # exit with 1 if function failed, else 0
            cmd += b";exit !$?"
        proc = subprocess.run(
            self.terrashell_path,
            input=cmd,
            capture_output=True,
            shell=False,
            check=False
        )
        self.logger.info(proc.stdout.decode())
        if proc.stderr:
            self.logger.error(proc.stderr.decode())

        # Invert return code to be pythonic
        if proc.returncode == 0:
            return True
        else:
            return False

    def get_vsphere_creds(self):
        cmd = b"Terra-Connect -ShowCreds"
        proc = subprocess.run(
            self.terrashell_path,
            input=cmd,
            capture_output=True,
            shell=False,
            check=False
        )
        res = proc.stdout.decode()
        # STDOUT also contains the command we typed and the powershell prompts,
        #   so strip that out
        #   (the result we want is json)
        try:
            # Drop everything before the first { and after }
            res = "{" + res.split("{", 1)[1].rsplit("}", 1)[0] + "}"
            # Don't log the creds, they might end up in a log file
            #self.logger.info(res)
            return json.loads(res)

        except Exception as e:
            self.logger.error(f"Failed to load hypervisor credentials! Does `echo 'Terra-Connect -ShowCreds' | terrashell` work?")
            self.logger.error(e)
            return None
        
    def _disconnect(self, cmd):
        """
        Safely attempt to disconnect, but catch exceptions
        """
        cmd_name = cmd.__class__.__name__
        try:
            cmd.disconnect()
        except Exception as e:
            self.logger.info(f"[{cmd_name}] Exception in disconnect: {e}")
        self.logger.debug(f"[{cmd_name}] Disconnected from vSphere.")
        
    def run_vsphere_cmd(self, cmd, vsphere_creds=None, extra_feedback=False):
        """
        Execute the vSphereCmd cmd.

        If extra_feedback is, increase log level for
            potentially long running operations.
        """
        feedback_level = logging.DEBUG
        if extra_feedback:
            feedback_level = logging.INFO
        if vsphere_creds:
            # User gave us explicit creds to use
            self.vsphere_creds = vsphere_creds

        msg_prefix = f"[{cmd.__class__.__name__}|{cmd.get_msg_tag()}]"
        if not self.vsphere_creds:
            # First hvcmd-style command;
            # need to get the raw hypervisor creds
            self.logger.debug(f"{msg_prefix} Loading hypervisor creds...")
            self.vsphere_creds = self.get_vsphere_creds()
        
        conn = None
        try:
            self.logger.log(feedback_level, f"{msg_prefix} Initializing...")
            conn = cmd.connect(
                self.vsphere_creds["vsphere_server"],
                self.vsphere_creds["vsphere_user"],
                self.vsphere_creds["vsphere_password"],
            )
            self.logger.debug(f"{msg_prefix} Initialized")
        except Exception as e:
            # Possible Causes:
            # * vsphere_creds are bad
            # * 'Terra-Connect -ShowCreds' failed due to bad terrshell intstall
            # * A requried resource does not exist / insufficient permissions
            self.logger.error(f"{msg_prefix} Exception in connect(): {e}")
            self.logger.error(traceback.format_exc())
            self._disconnect(cmd)
            return False
        
        if conn:
            self.logger.debug(f"{msg_prefix} Connect: OK")
        else:
            self.logger.error(f"{msg_prefix} Connect: FAILED")
            self._disconnect(cmd)
            return False
            
        # We should be connected now
        res = False
        try:
            self.logger.log(feedback_level, f"{msg_prefix} Running...")
            res = cmd.run()
            self.logger.debug(f"{msg_prefix} OK")
        except Exception as e:
            self.logger.error(f"{msg_prefix} Uncaught exception: {e}")
            self.logger.error(traceback.format_exc())

        return res

    def invoke_vsphere_cmd(self, cmd_class, *args, **kwargs):

        # Extract any arguments that are for (and not cmd_clas)
        for_terrashell = {
            key:kwargs[key] if key in kwargs else None
            for key in ["vsphere_creds", "extra_feedback"]
        }
        for key in list(for_terrashell):
            if key in kwargs:
                del kwargs[key]

        # If the caller did not pass a custom logger, use TerraShel's logger
        if "logger" not in kwargs:
            kwargs["logger"] = self.logger

        cmd = cmd_class(*args, **kwargs)
        self.logger.debug(f"[{cmd_class.__name__}] OK")
        return self.run_vsphere_cmd(cmd, **for_terrashell)

    ###########################################################################
    #       START VSPHERE CMDs
    ###########################################################################

    def get_used_vlans(self, *args, **kwargs):
        """
        Return a list containing all used VLAN ids for
            the given Datacenter and Distributed Virtual Switch.
        
        This can be used to find VLAN ids that are not in use
            (and thus can be given to a new 'isolated' port group.)
        Warning: changes to VLAN assignments before a new port group
            is created may cause a VLAN id to be reused.
        """
        return self.invoke_vsphere_cmd(GetUsedVLANs, *args, **kwargs)

    def get_vlan(self, *args, **kwargs):
        """
        Return the VLAN ID used by a network (port group).
        
        If the port group does not exist, returns None.
        """
        return self.invoke_vsphere_cmd(GetVLAN, *args, **kwargs)

    def get_vm_tags(self, *args, **kwargs):
        """
        Return the VM tags attached to this VM.
        
        If the vm does not exist, returns None.
        """
        return self.invoke_vsphere_cmd(GetVmTags, *args, **kwargs)

    def get_vm_by_tags(self, *args, **kwargs):
        """
        Return a list of VM names satifying the requested tag assignments.
        
        If no such VMs exist, returns None.
        If the requested tags or cateories do not exist, raises Exceptions.
        """
        return self.invoke_vsphere_cmd(GetVmByTags, *args, **kwargs)

    def get_mac_addresses(self, *args, **kwargs):
        """
        Return the Mac Addresses associated with this VM.
        
        If the vm does not exist, returns None.
        """
        return self.invoke_vsphere_cmd(GetMacAddresses, *args, **kwargs)

    def reboot_vm(self, *args, **kwargs):
        """
        Reboots the VM using Restart Guest OS (if possible) or Reset VM.

        If wait_for_tools, waits until VMware Tools are ready.
        
        If the vm does not exist, returns None.
        """
        return self.invoke_vsphere_cmd(RebootVM, *args, **kwargs)

    def get_vm(self, *args, **kwargs):
        """
        Gets a VM, optionally waiting for it to exist.

        Most properties of the returned object will not be accessible
            without an active connection.

        Returns None if vm does not exist and not waiting for it.
        """
        return self.invoke_vsphere_cmd(GetVm, *args, **kwargs)

    def vm_exists(self, *args, **kwargs):
        """
        Checks if a VM named vm_name exists within vm_folder (or a subfolder)
        """        
        """
        cmd = "Get-Path -Name {} | Get-VM -Name {}".format(
            shlex.quote(vm_folder),
            shlex.quote(vm_name),
        )
        """
        res = self.invoke_vsphere_cmd(GetVm, *args, **kwargs)
        return res is not None

    def guest_exec(self, *args, **kwargs):
        """
        Executes 'cmd' inside the Guest VM described by vm_info.

        See terrashell.guestcmd.GuestCmd for vm_info description.
        """
        return self.invoke_vsphere_cmd(GuestExec, *args, **kwargs)

    def guest_run_script(self, *args, **kwargs):
        """
        Uploads, executes, and deletes a script inside the Guest VM described by vm_info.

        See terrashell.guestcmd.GuestCmd for vm_info description.
        """
        return self.invoke_vsphere_cmd(GuestRunScript, *args, **kwargs)

    def guest_customize(self, *args, **kwargs):
        """
        Performs custom GuestOS customization on the target VM.

        See terrashell.guestcmd.GuestCmd for vm_info description.
        """
        # Give user more feedback that customization is happening
        return self.invoke_vsphere_cmd(
            GuestCustomize,
            *args,
            extra_feedback=True,
            **kwargs
        )

    def guest_download(self, *args, **kwargs):
        """
        Downloads the file at remote_path in the Guest VM described by vm_info.

        See terrashell.guestcmd.GuestCmd for vm_info description.
        """
        return self.invoke_vsphere_cmd(GuestDownload, *args, **kwargs)

    def guest_upload(self, *args, **kwargs):
        """
        Uploads the file at local_path to remote_path in the Guest VM described by vm_info.

        See terrashell.guestcmd.GuestCmd for vm_info description.
        """
        return self.invoke_vsphere_cmd(GuestUpload, *args, **kwargs)

    ###########################################################################
    #       END VSPHERE CMDs
    ###########################################################################

    def test_path(self, path):
        """
        Invoke terrashell, calling Test-Path

        This can be used to check if files exist on the datastore
        """
        cmd = "$res = Test-Path {}; exit !$res".format(shlex.quote(path))
        return self.run(cmd, set_returncode=False)

    def copy_datastoreitem(self, src, dest):
        """
        Invoke terrashell, calling Copy-DatastoreItem

        This can be used to upload or download files from the datastore
        """
        prefix = '$ProgressPreference = "SilentlyContinue"'
        # https://pubs.vmware.com/vsphere-50/index.jsp?topic=%2Fcom.vmware.powercli.cmdletref.doc_50%2FCopy-DatastoreItem.html
        # Force ensures parent folders get created as needed
        cmd = "{}; Copy-DatastoreItem -Force {} {}".format(
            prefix,
            shlex.quote(src),
            shlex.quote(dest)
        )
        return self.run(cmd)


    def remove_vm(self, vm_folder, vm_name):
        """
        Checks if a VM named vm_name exists in vm_folder
        [EXPERIMENTAL: DISABLED]
        """        
        if not (vm_folder and vm_name):
            raise ValueError(f"Refusing to delete {vm_folder}/{vm_name}")

        #TODO: Remove WhatIf
        cmd = "Get-Path -Name {} | Get-VM -Name {} | Remove-VM -DeletePermanently -WhatIf".format(
            shlex.quote(vm_folder),
            shlex.quote(vm_name),
        )
        self.logger.warn(f"Running deletion command: {cmd}")
        self.logger.warn("[remove_vm is disabled (-WhatIf) until it is confirmed to be safe]")

        return self.run(cmd)

    def apply_tags(self, vm_folder, vm_name, tags, force=True):
        """
        Applies all tags (dict of category, value pairs) to vm_name.

        Any missing categories or tags will be created as needed.
        """
        tag_names = ",".join(tags.values())
        tag_categories = ",".join(tags.keys())
        cmd = "Get-Path -Name {} | Get-VM -Name {}".format(
            shlex.quote(vm_folder),
            shlex.quote(vm_name),
        )
        cmd += f" | SafeNew-TagAssignment -TagNames {tag_names} -TagCategories {tag_categories}"
        if force:
            cmd += " -Force"
        
        return self.run(cmd)

    def prep_template(self, vm_folder, vm_name):
        """
        Does whatever vSphere operations are necessary to make a VM a template ready for copying.
    
        Ex) Enabling copy / paste, increasing video ram
        """        
        cmd = '$ProgressPreference = "SilentlyContinue"; Get-Path -Name {} | Get-VM -Name {} | Prep-Template'.format(
            shlex.quote(vm_folder),
            shlex.quote(vm_name),
        )
        
        return self.run(cmd)

    def enable_vmwarehardware(self, vm_folder, vm_name):
        """
        Sets VMWare Hardware (e.g. NetworkAdapater + SCSI controller) to the efficient VMware version

        (Useful if you had to bootstrap on old guest OS with generic hardware in order to install vmware tools)
        """        
        cmd = "Get-Path -Name {} | Get-VM -Name {} | Enable-VMwareHardware".format(
            shlex.quote(vm_folder),
            shlex.quote(vm_name),
        )
        
        return self.run(cmd)

    def enable_ovf_env(self, vm_folder, vm_name):
        """
        Enables the Ovf Environment, exposing additinal info to the GuestOS.

        (Useful if you need your mac addresses in vSphere-order, like pfSense does.)
        https://www.virtuallyghetto.com/2012/06/ovf-runtime-environment.html
        """        
        cmd = "Get-Path -Name {} | Get-VM -Name {} | Enable-OvfEnv".format(
            shlex.quote(vm_folder),
            shlex.quote(vm_name),
        )
        
        return self.run(cmd)
