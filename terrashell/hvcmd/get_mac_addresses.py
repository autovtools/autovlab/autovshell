from pyVmomi import vim  # pylint: disable=no-name-in-module

from terrashell.hvcmd import HvCmd

class GetMacAddresses(HvCmd):
    """
    Returns a list of MAC addresses (in order) that are associated with the VM.

    Inspired by:
        https://github.com/vmware/pyvmomi-community-samples/blob/master/samples/getvnicinfo.py
    """
    def __init__(self, **kwargs):
        super().__init__(vm_required=True, **kwargs)

        
    def get_mac_addresses(self):
        macs = []
        for dev in self.vm.config.hardware.device:
            if isinstance(dev, vim.vm.device.VirtualEthernetCard):
                macs.append(dev.macAddress)

        self.log_msg(f"MACs: {macs}", "DEBUG")
        return macs

    def run(self):
        if not self.vm:
            # vm does not exist
            return None

        return self.get_mac_addresses()
