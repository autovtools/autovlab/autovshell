from terrashell.hvcmd import HvCmd
from pyVmomi import vim  # pylint: disable=no-name-in-module

from com.vmware.vapi.std_client import DynamicID    # pylint: disable=import-error

from terrashell import vSphereCmd

class GetVmTags(HvCmd):
    """
    Returns a dictonary of the tags associated with this VM.

    Inspired by:
        https://github.com/vmware/vsphere-automation-sdk-python/blob/master/samples/vsphere/tagging/tagging_workflow.py
    """
    def __init__(self, **kwargs):
        super().__init__(rest=True, vm_required=True, **kwargs)

    def _parse_tags(self, attached_tags):
        # key = tag category
        # val = tag name
        res = {}
        for tag_id in attached_tags:
            tag = self.client.tagging.Tag.get(tag_id)
            category = self.client.tagging.Category.get(tag.category_id)
            res[category.name] = tag.name

        return res

    def _get_tags(self):
        dynamic_id = DynamicID(type=self.vm._wsdlName, id=self.vm._moId)
        attached_tags = self.client.tagging.TagAssociation.list_attached_tags(
            dynamic_id
        )
        tags = self._parse_tags(attached_tags)
        return tags

    def run(self):
        if not self.vm:
            # VM does not exist
            return None

        tags = self._get_tags()
        self.log_msg(tags, "DEBUG")
        return tags
