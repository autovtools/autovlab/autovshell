import time
from gevent import sleep, GreenletExit

from pyVmomi import vim  # pylint: disable=no-name-in-module

from terrashell import vSphereCmd
from terrashell.hvcmd import HvCmd

class GetVm(HvCmd):
    """
    Gets a VM, optionally waiting for it to exist.

    Most properties of the returned object will not be accessible
        without an active connection.

    Inspired by:
        https://github.com/vmware/pyvmomi-community-samples/tree/master/samples
    """
    def __init__(self, wait_for_exists=False, **kwargs):
        super().__init__(vm_required=False, **kwargs)
        self.wait_for_exists = wait_for_exists

    def validate(self):
        # Since we might be waiting for the VM to exist,
        #   vm_required is False, but we want strict validation
        #   for vm_info
        vSphereCmd.validate_vm_info(self.vm_info)

    def run(self):
        start_time = time.time()
        while True:
            try:
                self.get_vm()
            except GreenletExit as e:
                raise e
            except Exception as e:
                # Catch any random vSphere faults
                #   (just in case)
                self.log_msg(f"Exception searching for vm: {e}", "ERROR")

            if self.vm:
                self.log_msg(f"Found", "DEBUG")
                return self.vm

            # Assert VM does not exist
            if not self.wait_for_exists:
                # User doesn't want to wait
                self.log_msg(f"Not Found", "DEBUG")
                return None

            # Assert user wants to wait for this VM
            vm_name = self.vm_info.get("vm_name", None)
            if not vm_name:
                msg = "Waiting for a VM by Id will block forever"
                self.log_msg(msg, "ERROR")
                raise ValueError(msg)

            self.check_timeout(start_time)
            self.log_msg(f"Waiting for {vm_name} to exist...", "DEBUG")
            sleep(self.sleep_duration)


