import logging
import traceback
import requests

from gevent import sleep

# Some operations require REST api:
#    https://github.com/vmware/vsphere-automation-sdk-python
from vmware.vapi.vsphere.client import create_vsphere_client    # pylint: disable=import-error
from com.vmware.cis_client import Session

from terrashell import vSphereCmd

class HvCmd(vSphereCmd):

    def __init__(self, client=None, rest=False, **kwargs):
        """
        vm_reqired: If True, vm_info is strictly validated and self.vm is set
        rest: If true, this command uses the REST API
                and connect should create a REST client
        client: A connected REST client to reuse
        """
        super().__init__(**kwargs)
        self.client = client
        self.rest = rest

    def _get_unverified_session(self):
        # https://github.com/vmware/vsphere-automation-sdk-python/blob/master/samples/vsphere/common/ssl_helper.py
        session = requests.session()
        session.verify = False
        requests.packages.urllib3.disable_warnings() # pylint: disable=no-member
        return session

    def connect(self, host, user, pwd, port=443):
        super().connect(host, user, pwd, port=port)

        if self.rest and not self.client:
            # Give REST sessions a REST client + normal connection
            session = self._get_unverified_session()
            self.client = create_vsphere_client(
                server=host,
                username=user,
                password=pwd,
                session=session
            )

        return self.conn

# You will hit session limit if you don't disconnect your REST clients
# (None of the examples disconnect)
# https://github.com/vmware/vsphere-automation-sdk-python/blob/master/samples/vsphere/common/vapiconnect.py#L80
    def disconnect(self):
        super().disconnect()
        if self.rest:
            # Disconnect the REST session
            sess = Session(self.client._stub_config)
            sess.delete()
            self.client = None
