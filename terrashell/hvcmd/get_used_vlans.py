from pyVmomi import vim  # pylint: disable=no-name-in-module

from terrashell.hvcmd import HvCmd

class GetUsedVLANs(HvCmd):
    """
    Returns a list of all VLAN ids that are in use on the requsted dvs.

    Inspired by https://github.com/vmware/pyvmomi-community-samples/blob/master/samples/list_vlan_in_portgroups.py
    """
    def __init__(self, dc, dvs=None, **kwargs):
        super().__init__(**kwargs)
        self.dc = dc
        self.dvs = dvs

    def run(self):
        content = self.conn.RetrieveContent()
        dc = self.get_obj(content, [vim.Datacenter], self.dc)
        if not dc:
            raise Exception(f"Failed to get Datacenter {self.dc}.")

        if self.dvs is None:
            dvs_lists = self.get_obj(content, [vim.DistributedVirtualSwitch],
                                folder=dc.networkFolder)
        else:
            dvsn = self.get_obj(content, [vim.DistributedVirtualSwitch], self.dvs)
            if dvsn is None:
                raise Exception(f"Failed to get Distributed Virutal Switch {self.dvs}.")
            else:
                dvs_lists = [dvsn]

        res = []
        for dvs in dvs_lists:
            for dvs_pg in dvs.portgroup:
                vlanInfo = dvs_pg.config.defaultPortConfig.vlan
                cl = vim.dvs.VmwareDistributedVirtualSwitch.TrunkVlanSpec
                if not isinstance(vlanInfo, cl):
                    # Ignore trunks
                    res.append(vlanInfo.vlanId)
        return res

