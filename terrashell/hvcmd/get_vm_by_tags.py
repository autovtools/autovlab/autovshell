from terrashell.hvcmd import HvCmd
from pyVmomi import vim  # pylint: disable=no-name-in-module

from com.vmware.vapi.std_client import DynamicID    # pylint: disable=import-error

from terrashell import vSphereCmd

class GetVmByTags(HvCmd):
    """
    Returns a list of VMs matching the requested tags, optionally sorted.

    Restrict the filter critieria as much as possible, or you will hit the vSphere API Rate Limit.
    (https://docs.vmware.com/en/VMware-Integrated-OpenStack/4.1/com.vmware.openstack.admin.doc/GUID-5075DFBF-011A-4347-8AF5-17E760D62D8D.html)

    Inspired by:
        https://github.com/vmware/vsphere-automation-sdk-python/blob/master/samples/vsphere/tagging/tagging_workflow.py
        https://vmware.github.io/vsphere-automation-sdk-python/vsphere/6.7.0/com.vmware.cis.html?highlight=tagging
    """
    def __init__(self, tags={}, sort_by=[], **kwargs):
        super().__init__(rest=True, vm_required=False, **kwargs)

        self.tags = {
            key:val
            if isinstance(val, list) else [val]
            for key, val in tags.items()
        }
        self.sort_by = sort_by
        self.sort_by_ids = None

    def get_msg_tag(self):
        # We don't have a VM, so decorate our log messages with the tags we are filtering for
        return f"{self.tags}"

    def _get_matching_tags(self, category):
        """
        Returns a list of tag_ids in the specified category that match the filter.

        """
        matches = []
        tag_ids = self.client.tagging.Tag.list_tags_for_category(
            category.id
        )
        for tag_id in tag_ids:
            tag = self.client.tagging.Tag.get(tag_id)
            if tag.name in self.tags[category.name]:
                # This tag meets the filter criteria
                matches.append(tag_id)
        return matches

    def _get_categories_by_name(self, category_names):
        categories = []
        num_found = 0
        for category_id in self.client.tagging.Category.list():
            category = self.client.tagging.Category.get(category_id)
            if category.name in category_names:
                index = category_names.index(category.name)
                categories.insert(index, category)
                num_found += 1
                if num_found == len(category_names):
                    break
        return categories
        
    def _get_tag_map(self):
        tag_map = {}
        # Find the tags that satisfy our criteria
        # Get the categories we care about
        categories = self._get_categories_by_name(list(self.tags))
        for category in categories:
            # Get list of tags in this category that meet our filter criteria
            self.log_msg(f"Filtering on {category.name}","DEBUG")
            tag_map[category] = self._get_matching_tags(category)
            if not tag_map[category]:
                raise ValueError(
                    f"Category {category.name} does not contain any tags matching filter criteria!"
                )

        return tag_map

    def _get_matching_vms(self, tag_map):
        # A matching VM will contain at least one tag from each list in tag_map
        vm_ids = None
        for category, tag_ids in tag_map.items():
            curr_ids = []
            matches = self.client.tagging.TagAssociation.list_attached_objects_on_tags(
                tag_ids
            )
            # https://vmware.github.io/vsphere-automation-sdk-python/vsphere/6.7.0/com.vmware.cis.html?highlight=tagging
            #   #com.vmware.cis.tagging_client.TagAssociation.list_attached_objects_on_tags
            for match in matches:
                for dynamic_id in match.object_ids:
                    curr_ids.append(dynamic_id.id)

            self.log_msg(f"{len(curr_ids)} VMs have matching {category.name}", "DEBUG")
            if vm_ids is None:
                # First interation; no one is disqualified
                vm_ids = curr_ids
            else:
                # Any remaining VMs that do not also match on this category are elimated
                vm_ids = [vm_id for vm_id in vm_ids if vm_id in curr_ids]

            self.log_msg(f"{len(vm_ids)} VMs match so far.", "DEBUG")
            # Nothing matches; we can stop searching
            if not vm_ids:
                break

        return vm_ids

    def _sort_key(self, vm):
        # Returns a sortable tuple of tag values (based on self.sort_by)

        dynamic_id = DynamicID(type=vm._wsdlName, id=vm._moId)
        tag_ids = self.client.tagging.TagAssociation.list_attached_tags(
            dynamic_id
        )
        # Find the tag in the right category
        cmp_tuple = [""] * len(self.sort_by)
        for index, category in enumerate(self.sort_by_categories):
            for tag_id in tag_ids:
                tag = self.client.tagging.Tag.get(tag_id)
                if tag.category_id == category.id:
                    val = tag.name
                    if "version" in category.name:
                        # Make it a comparable version
                        try:
                            val = list([int(tok) for tok in tag.name.split(".")])
                        except Exception as e:
                            self.log_msg(
                                f"Unable to sort {category.name} with value {tag.name} as a numeric version",
                                "WARNING"
                            )
                        
                    cmp_tuple[index] = val

        self.log_msg(f"{vm.name} => {cmp_tuple}", "DEBUG")
        return cmp_tuple

    def get_vm_by_tags(self):
        tag_map = self._get_tag_map()
        vm_ids = self._get_matching_vms(tag_map)
        self.log_msg(f"vm_ids: {vm_ids}", "DEBUG")
        vms = [self.get_vm_by_moid(vm_id) for vm_id in vm_ids]

        if self.sort_by:
            # Precompute cateogries for efficiency
            self.sort_by_categories = self._get_categories_by_name(self.sort_by)
            self.log_msg(f"Sorting by: {self.sort_by}", "DEBUG")
            vms = sorted(vms, key=self._sort_key)

        vm_names = [vm.name for vm in vms] 
        self.log_msg(f"vm_names: {vm_names}", "DEBUG")
        return vm_names
    
    def run(self):
        return self.get_vm_by_tags()
