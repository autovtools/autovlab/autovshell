from terrashell.hvcmd import HvCmd
from pyVmomi import vim  # pylint: disable=no-name-in-module

class GetVLAN(HvCmd):
    """
    Returns the VLAN id of a network (port group).

    Inspired by:
        https://github.com/vmware/pyvmomi-community-samples/blob/master/samples/getvnicinfo.py
        https://github.com/vmware/pyvmomi-community-samples/blob/master/samples/add_nic_to_vm.py
    """
    def __init__(self, network, **kwargs):
        super().__init__(**kwargs)
        self.network = network

    def run(self):
        content = self.conn.RetrieveContent()
        network = self.get_obj(content, [vim.Network], self.network)

        if not network:
            # Network does not exist
            # (No VLAN = 0)
            self.log_msg(f"{network} not found", "DEBUG")
            return None
        vlanId = network.config.defaultPortConfig.vlan.vlanId 
        return vlanId
