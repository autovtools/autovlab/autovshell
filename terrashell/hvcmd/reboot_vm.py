from gevent import sleep, GreenletExit

from pyVmomi import vim  # pylint: disable=no-name-in-module
from pyVim.task import WaitForTask

from terrashell.hvcmd import HvCmd

class RebootVM(HvCmd):
    """
    Reboots the VM, using Restart Guest OS (if available), or Reset VM.

    Inspired by:
        https://github.com/vmware/pyvmomi-community-samples/blob/master/samples/reboot_vm.py
        https://github.com/sijis/pyvmomi-examples/blob/master/reboot-vm.py
    """
    def __init__(self, wait_for_reboot=True, wait_for_tools=True, **kwargs):
        super().__init__(vm_required=True, **kwargs)
        self.wait_for_reboot = wait_for_reboot
        self.wait_for_tools = wait_for_tools

    def _power_on_vm(self):
        try:
            task = self.vm.PowerOn()
            WaitForTask(task)
            return True
        except GreenletExit as e:
            raise e
        except Exception as e:
            self.log_msg("[PowerOn] failed:", "WARNING")

        return False

    def _reboot_guest(self):
        try:
            status = self.vm.guest.toolsRunningStatus
            if status != "guestToolsRunning":
                self.log_msg(f"[RebootGuest] Skipping because status: {status}", "DEBUG")
                return False
            self.vm.RebootGuest()
            return True
        except GreenletExit as e:
            raise e
        except Exception as e:
            self.log_msg("[RebootGuest] Failed:", "WARNING")
            self.log_msg(e, "WARNING")

        return False

    def _reset_vm(self):
        try:
            if self.vm.runtime.powerState != "poweredOn":
                self.log_msg("[RebootGuest] Will fail because VM is not poweredOn.")
                res = self._power_on_vm()
                if res and self.vm.runtime.powerState == "poweredOff":
                    # Consider powering on a VM that is off the same as a reset
                    return True
            task = self.vm.ResetVM_Task()
            WaitForTask(task)
            return True
        except GreenletExit as e:
            raise e
        except Exception as e:
            self.log_msg("[ResetVM] Failed:", "WARNING")
            self.log_msg(e, "WARNING")

        return False

    def reboot(self):
        old_boot = self.vm.runtime.bootTime
        if self._reboot_guest() or self._reset_vm():
            if self.wait_for_reboot:
                # Wait until VM power cycles
                # Useful if Restart Guest OS is slow
                self.wait_until_boot(old_boot)

            if self.wait_for_tools:
                # Wait until VMware tools start running
                self.wait_until_tools_ready()
            return True

        return False

    def run(self):
        if not self.vm:
            # VM does not exist
            return None

        if self.vm.runtime.powerState == "suspended":
            # Give RebootGuest a chance to work by resuming first
            self.log_msg("[RebootVM] Resuming suspended VM")
            self.ensure_on()
            
        return self.retry(self.reboot, require_true=True)
