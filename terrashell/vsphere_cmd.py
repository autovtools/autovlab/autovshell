import os
import time
import logging
import traceback

from gevent import sleep, GreenletExit

from pyVmomi import vim     # pylint: disable=no-name-in-module
from pyVim.task import WaitForTask
from pyVim.connect import SmartConnect, SmartConnectNoSSL, Disconnect

class vSphereCmd():
    """Base class for commands that use vSphere API calls.

    vm_info example:
    {
        "vm_name":"MyVM",
        "vm_moid":"vm-1234",
        "vm_user":"MyUser",
        "vm_password":"MyPassword"
    }
    Notes:
        * only one of vm_name and vm_moid are required.
        * vm_user / vm_password are only needed for some commands
            (typically GuestCmd's)
    """
    def __init__(self, vm_info=None, vm_name=None,
        desired_power_state=None, vm_required=False,
        sleep_duration=10, timeout=300, num_retries=0,
        root_folder=None, conn=None, vm=None, logger=None,
    ):
        # Standard args
        self.vm_info = vm_info
        if not self.vm_info:
            self.vm_info = {}

        # Behavior modifiers
        self.vm_required = vm_required
        self.sleep_duration = sleep_duration
        self.timeout = timeout
        self.desired_power_state = desired_power_state
        self.num_retries = num_retries
        # Contrains get_vm, etc. to only search relative to root_folder
        self.root_folder = root_folder

        # Internal objects that can be shared
        self.vm = vm
        self.conn = conn
        self.logger = logger

        # Give user optional name-only syntax
        #   Normalize it to vm_info
        if vm_name and "vm_name" not in self.vm_info:
            self.vm_info["vm_name"] = vm_name

        self.validate()

    def validate(self):
        if self.vm_required:
            self.validate_vm_info(self.vm_info)

    @staticmethod
    def validate_vm_info(vm_info):
        if not ("vm_name" in vm_info or "vm_moid" in vm_info):
            raise ValueError("vm_info must contain vm_name or vm_moid")

    @staticmethod
    def validate_vm_creds(vm_info):
        if not ("vm_user" in vm_info and "vm_password" in vm_info):
            raise ValueError("vm_info must contain vm_user and vm_password")

    
    @staticmethod
    def get_folder(content, folder_path):
        folders = folder_path.split(os.path.sep)
        curr = None
        for folder in folders:
            if not folder:
                # Ignore trailing slash
                continue
            # Search for the next subfolder relative to this one
            curr = vSphereCmd.get_obj(content, [vim.Folder], folder, folder=curr)
            if curr is None:
                return None
        return curr

    @staticmethod
    def get_obj(content, vimtype, name=None, folder=None, recurse=True):
        if not folder:
            folder = content.rootFolder
        elif isinstance(folder, str):
            requested = folder
            folder = vSphereCmd.get_folder(content, folder)
            if not folder:
                raise ValueError(f"The requested folder '{requested}' could not be found!")
        obj = None
        container = content.viewManager.CreateContainerView(folder,
                                                            vimtype, recurse)
        if not name:
            obj = {}
            for managed_object_ref in container.view:
                obj.update({managed_object_ref: managed_object_ref.name})
        else:
            obj = None
            for c in container.view:
                if c.name == name:
                    obj = c
                    break

        return obj

    def get_msg_tag(self):
        """
        Returns a str 'tag' that can be added to logging messages to include vm info.

        """
        tag = ""
        if self.vm:
            tag = f"{self.vm.name}|{self.vm._moId}"
        else:
            if "vm_name" in self.vm_info:
                tag += self.vm_info["vm_name"]
            if "vm_moid" in self.vm_info:
                if tag:
                    tag += "|"
                tag += self.vm_info["vm_moid"]

        return tag

    def get_vm(self):
        vm_id = None
        if "vm_moid" in self.vm_info:
            vm_id = self.vm_info["vm_moid"]
            self.vm = self.get_vm_by_moid(vm_id)
        elif "vm_name" in self.vm_info:
            vm_id = self.vm_info["vm_name"]
            self.vm = self.get_vm_by_name(vm_id)
        else:
            msg = "vm_info missing vm_moid / vm_name!"
            self.log_msg(msg, "ERROR")
            raise ValueError(msg)

        if not self.vm and self.vm_required:
            # Fatal
            msg = f"{vm_id}: Not Found!"
            self.log_msg(msg, "ERROR")
            raise ValueError(msg)

        desired = self.desired_power_state
        if desired:
            if desired  == "poweredOn":
                self.ensure_on()
            elif desired == "poweredOff":
                self.ensure_off()
            elif desired == "suspended":
                self.ensure_suspended()
            else:
                # https://pubs.vmware.com/vi3/sdk/ReferenceGuide/vim.VirtualMachine.PowerState.html
                msg = f"desired_power_state: {desired} does not exist"
                self.log_msg(msg, "ERROR")
                raise ValueError(msg)
            
        return self.vm

    def get_vm_by_name(self, vm_name):
        content = self.conn.RetrieveContent()
        vm = self.get_obj(content, [vim.VirtualMachine], vm_name, folder=self.root_folder)
        if not vm:
            self.log_msg(f"{vm_name} not found", "DEBUG")
        return vm

    def get_vm_by_moid(self, moid):
        vm = vim.VirtualMachine(moid)
        vm._stub = self.conn._stub
        try:
            name = vm.name
            return vm
        
        except GreenletExit as e:
            raise e
        except Exception as e:
            # If we have an invalid ID, it's bad enough to warn the user
            self.log_msg(
                f"Failed to get vm by Managed Object ID: {e}",
                "WARNING"
            )
        
        return None

    def connect(self, host, user, pwd, port=443):
        if not self.conn:
            self.conn = SmartConnectNoSSL(
                host=host,
                user=user,
                pwd=pwd,
                port=port
            )
        if self.vm_required:
            self.get_vm() 
        
        return self.conn

    def disconnect(self, *args, **kwargs):
        if self.conn:
            Disconnect(self.conn)
            self.conn = None

    def run(self):
        raise NotImplementedError()

    def log_msg(self, msg, level="INFO"):
        if not self.logger:
            return

        msg_level = level
        try:
            if isinstance(level, str):
                level = int(logging.getLevelName(level))

            # Try string conversion, default to type if that fails
            try:
                msg = f"{msg}"

            except GreenletExit as e:
                raise e
            except Exception as e:
                msg = f"<{type(msg)}>"

            # Tack on some useful caller info
            prefix = self.__class__.__name__
            if self.vm:
                prefix += f"|{self.get_msg_tag()}"
            prefix = f"[{prefix}]"
            msg = f"{prefix} {msg}" 

            self.logger.log(level, msg)
        except GreenletExit as e:
            raise e
        except Exception as e:
            # We probably have a typo above, or formatting broke
            self.logger.error("-"*80)
            self.logger.error(traceback.format_exc())
            self.logger.error("-"*80)
            self.logger.error(f"Unable to log {msg_level} message: {e}")
            self.logger.error("-"*80)

    def check_timeout(self, start_time, timeout=None):
        if not timeout:
            # Default timeout
            timeout = self.timeout
        if not timeout:
            # No timeout
            return
        if time.time() - start_time > self.timeout:
            raise Exception("Guest Timeout Expired")

    def wait_until_power_state(self, desired, timeout=None):
        if not timeout:
            timeout = self.timeout
            
        start_time = time.time()
        while True:
            try:
                curr = self.vm.runtime.powerState

                if curr == desired:
                    self.log_msg(f"Reached desired state: {desired}", "DEBUG")
                    return
                self.log_msg(f"Waiting for {desired} (current: {curr})", "DEBUG")
                sleep(self.sleep_duration)
            except GreenletExit as e:
                raise e
            except Exception as e:
                try:
                    self.log_msg(f"Exception waiting for {desired}:")
                    self.log_msg(e)
                finally:
                    sleep(self.sleep_duration)

            self.check_timeout(start_time, timeout=timeout)
        
    def wait_until_tools_ready(self, timeout=None):
        if not timeout:
            timeout = self.timeout
        start_time = time.time()
        while True:
            try:
                status = self.vm.guest.toolsRunningStatus
                self.log_msg(status, "DEBUG")

                if status == "guestToolsRunning":
                    return
                    
                sleep(self.sleep_duration)
            except GreenletExit as e:
                raise e
            except Exception as e:
                try:
                    self.log_msg("[wait_until_tools_ready] Exception:")
                    self.log_msg(e)
                finally:
                    sleep(self.sleep_duration)

            self.check_timeout(start_time, timeout=timeout)
        
    def wait_until_boot(self, old_boot, timeout=None):
        if not timeout:
            timeout = self.timeout
        start_time = time.time()
        while old_boot == self.vm.runtime.bootTime:
            msg = "Waiting for boot..."
            self.log_msg("Waiting for boot...", "DEBUG")
            sleep(self.sleep_duration)
            self.check_timeout(start_time, timeout=timeout)

        self.log_msg(f"Last Boot: {self.vm.runtime.bootTime} (Was: {old_boot})", "DEBUG")

    def wait_until_guest_operations_ready(self, timeout=None):
        # https://communities.vmware.com/message/2884901#2884901
        # https://code.vmware.com/apis/968/vsphere
        if not timeout:
            timeout = self.timeout
        start_time = time.time()
        while True:
            try:
                ready = self.vm.guest.guestOperationsReady
                self.log_msg(f"guestOperationsReady: {ready}", "DEBUG")
                if ready:
                    return
                sleep(self.sleep_duration)
            except GreenletExit as e:
                raise e
            except Exception as e:
                self.log_msg("[wait_until_guest_operations_ready] Exception:", "WARNING")
                self.log_msg(e, "WARNING")
                sleep(self.sleep_duration)

            self.check_timeout(start_time, timeout=timeout)

    def wait_until_guest_ip(self, timeout=None):
        if not timeout:
            timeout = self.timeout
        start_time = time.time()
        while True:
            try:
                ipAddress = self.vm.guest.ipAddress
                self.log_msg(f"Guest IP: {ipAddress}", "DEBUG")
                if ipAddress:
                    return
                sleep(self.sleep_duration)
            except GreenletExit as e:
                raise e
            except Exception as e:
                self.log_msg("[wait_until_guest_ip] Exception:", "WARNING")
                self.log_msg(e, "WARNING")
                sleep(self.sleep_duration)

            self.check_timeout(start_time, timeout=timeout)

    def get_contents(self, file_path, file_contents, fatal=True):
        """
        Returns file_contents (if set) or the the contents of file_path

        """
        if not (file_path or file_contents):
            if fatal:
                raise ValueError("Either a file path or file contents are required !") 
            else:
                return None

        if file_contents:
            return file_contents

        with open(file_path, "rb") as file_obj:
            return file_obj.read()

    def is_state(self, power_state):
        return self.vm.runtime.powerState == power_state

    def ensure_on(self):
        desired = vim.VirtualMachinePowerState.poweredOn
        if self.is_state(desired):
            return True
        # (Race condition; best effort)
        try:
            task = self.vm.PowerOn()
            WaitForTask(task)
            self.wait_until_power_state(desired)
            return True
        except GreenletExit as e:
            raise e
        except Exception as e:
            self.log_msg("[PowerOn] Error:", "WARNING")
            self.log_msg(e, "WARNING")
        return False

    def ensure_off(self, force=False):
        desired = vim.VirtualMachinePowerState.poweredOff
        if self.is_state(desired):
            return True
        # (Race condition; best effort)

        tools_status = self.vm.guest.toolsRunningStatus
        # Force = skip nice shutdown
        if not force and tools_status == "guestToolsRunning":
            try:
                # Try to do a guest shutdown
                task = self.vm.ShutdownGuest()
                WaitForTask(task)
                self.wait_until_power_state(desired)
                return True
            except GreenletExit as e:
                raise e
            except Exception as e:
                self.log_msg("[ShutdownGuest] Error:", "WARNING")
                self.log_msg(e, "WARNING")

        try:
            # If needed, force off
            task = self.vm.PowerOff()
            WaitForTask(task)
            self.wait_until_power_state(desired)
            return True
        except GreenletExit as e:
            raise e
        except Exception as e:
            self.log_msg("[PowerOff] Error:", "WARNING")
            self.log_msg(e, "WARNING")
        
        return False

    def ensure_suspended(self):
        desired = vim.VirtualMachinePowerState.suspended
        if self.is_state(desired):
            return True
        # (Race condition; best effort)

        tools_status = self.vm.guest.toolsRunningStatus
        if tools_status == "guestToolsRunning":
            try:
                # Warn the guest it is being suspended
                task = self.vm.StandbyGuest()
                WaitForTask(task)
            except GreenletExit as e:
                raise e
            except Exception as e:
                self.log_msg("[StandbyGuest] Error:", "WARNING")
                self.log_msg(e, "WARNING")

        try:
            task = self.vm.Suspend()
            WaitForTask(task)
            self.wait_until_power_state(desired)
            return True
        except GreenletExit as e:
            raise e
        except Exception as e:
            self.log_msg("[Suspend] Error:", "WARNING")
            self.log_msg(e, "WARNING")
        return False

    def retry(self, func, msg_prefix="", require_true=False, backoff=10):
        while True:
            try:
                res = func()
                if require_true and not res:
                    # False is fatal
                    msg = f"{msg_prefix} returned {res}"
                    raise Exception(msg)
                # Success
                return res
                    
            except GreenletExit as e:
                raise e
            except Exception as e:
                self.log_msg(f"{msg_prefix}Failed ({self.num_retries} attempts remaining):", "WARNING")
                self.log_msg(e, "WARNING")
     
                can_retry = False
                if self.num_retries is None:
                    self.log_msg(f"{msg_prefix}Retrying until success...", "DEBUG")
                    can_retry = True
                if self.num_retries > 0:
                    self.num_retries -= 1
                    can_retry = True

                if can_retry:
                    sleep(self.sleep_duration)
                    self.sleep_duration += backoff
                    continue

                # Out of retries; rethrow to caller
                raise e
