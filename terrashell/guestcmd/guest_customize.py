from gevent import sleep, GreenletExit

from terrashell.guestcmd import GuestCmd
from terrashell.hvcmd import RebootVM

from .guest_run_script import GuestRunScript

class GuestCustomize(GuestCmd):
    """
    Performs custom GuestOS customization on the target VM.

    The actual customization logic and data is provided elsewhere;
        this just kicks off the customization process.

    Inspired by:
        https://github.com/vmware/vsphere-guest-run/blob/master/vsphere_guest_run/vsphere.py
    """

    # Signature similar to GuestRunScript
    def __init__(self, local_path=None, script_contents=None,
        remote_path=None, reboot_after=True, **kwargs
    ):
        # Unless the user explictly tells us to wait for guest tools, don't
        # wait_for_guest => connect will fail and we will never get to run()
        if not "wait_for_guest" in kwargs:
            kwargs["wait_for_guest"] = False

        kwargs["desired_power_state"] = "poweredOn"

        super().__init__(vm_required=True, **kwargs)
        if not "num_retries" in kwargs:
            self.num_retries = 1
        # Save the extra args, so we can pass them to helpers
        self.kwargs = kwargs
        self.reboot_after = reboot_after
        # We need a connection to vSphere, share it with our helpers
        # We have to delay their creation until after connect() was called
        self.runner = None
        self.rebooter = None

        self.local_path = local_path
        self.script_contents = script_contents
        self.remote_path = remote_path
        
    def _safe_except(self, exc):
        """
        Print out an exception warning very carefully, in case formatting throws another exception.
        """
        try:
            self.log_msg("Exception:", "WARNING")
            self.log_msg(exc, "WARNING")
        except GreenletExit as e:
            raise e
        except Exception:
            # log_msg already tried it's best;
            # Something is probalby broken with string formatting
            #   do *not* throw another exception
            pass

    def connect(self, *args, **kwargs):
        # Connect as usual
        conn = super().connect(*args, **kwargs)
        if conn:
            # We are connected, create our helpers
            for key in ["conn", "vm_info", "vm", "guest_auth"]:
                self.kwargs[key] = getattr(self, key)

            # We need more control over timeouts
            self.kwargs["wait_for_guest"] = False
            if "num_retries" not in self.kwargs:
                # Let each sub-command retry on its own
                #   before we give up and reset
                # WARNING: older FreeBSD / pfSense will fail a lot:
                # https://github.com/vmware/open-vm-tools/issues/236
                # Packinator preinstalls a watchdog to restart vmtoolsd for us
                self.kwargs["num_retries"] = 5

            self.kwargs["wait_for_guest"] = False

            self.log_msg("[HELPER] GuestRunScript", "DEBUG")
            self.runner = GuestRunScript(
                local_path=self.local_path,
                script_contents=self.script_contents,
                remote_path=self.remote_path,
                **self.kwargs
            )
            self.runner.connect(*args, **kwargs)

            # HvCmd doesn't understand some GuestCmd only options
            _kwargs = self.kwargs.copy()
            for key in ["guest_auth", "wait_for_guest"]:
                del _kwargs[key] 

            self.log_msg("[HELPER] RebootVM", "DEBUG")
            self.rebooter = RebootVM(
                self.vm.name,
                **_kwargs
            )
            self.rebooter.connect(*args, **kwargs)
        
        return conn

    def check_result(self):
        customized = False
        res = self.runner.run()
        
        if res and res.get("return_code", 1) == 0:
            customized = True
        else:
            msg = f"Customizer failed with result: {res}"
            raise Exception(msg)
        
        return res, customized

    def customize(self):
        # If things get really weird / bad, could block forever.
        #   but the user is counting on us to do this Customization,
        #   so keep trying (but slow down a bit)!
        retry_delay = self.sleep_duration
        customized = False
        res = None
        while True:
            rebooted = False
            try:
                # Ensure guestOperations are ready if we rebooted
                self.log_msg("Waiting for Guest Operations", "DEBUG")
                self.wait_until_guest_operations_ready()
                # For follow-up customizers, waiting until the IP address has been set is a good idea
                if self.wait_for_ip:
                    self.wait_until_guest_ip()
                    
                self.log_msg("Guest Operations Ready", "DEBUG")

                # Try to run the customization script
                self.log_msg("Running Customizer", "DEBUG")
                res, customized = self.retry(self.check_result)
                self.log_msg("Customizer returned", "DEBUG")

            except GreenletExit as e:
                raise e
            except Exception as e:
                self.log_msg("Got Exception", "DEBUG")
                self._safe_except(e)
                # An exception occurred
            finally:
                # Always executed; after attempted customization

                if customized and not self.reboot_after:
                    # We are done; success
                    # Customized, but did not reboot
                    self.log_msg("Customization complete", "INFO")
                    return res

                # We need to reboot
                #   (either cust failed or we need to reboot to apply)
                try:
                    # Try to wait for guest operations to come back so we don't
                    #   corrupt the OS by force resetting
                    # Also pfSense might detect failure and revert the config
                    self.try_wait_tools()
                    rebooted = self.rebooter.run()
                    # If we get here, pretty confident reboot happened
                    if customized:
                        # We are done; success
                        # Customized and rebooted
                        self.log_msg("Customization complete", "INFO")
                        return res

                except GreenletExit as e:
                    raise e
                except Exception as e:
                    # Log reboot exceptions, but continue
                    #   (We don't want to get stuck in a reboot loop)
                    self._safe_except(e)

                # We got at least one exception and have attempted a reset
                #   Sleep and try again
                self.log_msg(f"Customized: {customized} | Rebooted: {rebooted}", "WARNING")
                self.log_msg(f"Trying again in {retry_delay} seconds...", "WARNING")
                sleep(retry_delay)

                # Back off a bit, in case the vSphere server is overwhelmed
                retry_delay += 5
                self.sleep_duration = retry_delay
                self.runner.sleep_duration = retry_delay
                self.rebooter.sleep_duration = retry_delay


    def try_wait_tools(self, timeout=60):
        try:
            self.wait_until_tools_ready(timeout=timeout)
        except GreenletExit as e:
            raise e
        except Exception:
            self._safe_except("Tools not running after customization attempt.")

    def run(self):
        return self.customize()
