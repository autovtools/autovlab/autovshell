import logging
import requests
import traceback
import time

from gevent import sleep

from pyVim.connect import Disconnect 
from pyVmomi import vim     # pylint: disable=no-name-in-module

# TODO: Eliminate Depepency
# TODO: Add Windows support

from terrashell import vSphereCmd

class GuestCmd(vSphereCmd):
    """
    Commands that leverage hypervisor access to perform operations
        on / in the Guest VM.

    VMware tools are *required* on the Guest for these commands to work.
    VM credentials are required in vm_info.
    """

    def __init__(self, wait_for_guest=True, wait_for_ip=False, guest_auth=None, **kwargs):
        if not "vm_required" in kwargs:
            kwargs["vm_required"] = True

        super().__init__(**kwargs)
        self.guest_auth = guest_auth
        self.wait_for_guest = wait_for_guest
        # This helps ensure we let vmtools guest customization finish first
        self.wait_for_ip = wait_for_ip
        
    def validate(self):
        # GuestCmds need vm identifier + guest creds
        vSphereCmd.validate_vm_info(self.vm_info) 
        vSphereCmd.validate_vm_creds(self.vm_info) 

    def connect(self, host, user, pwd, port=443):
        """
        Ensure we are connected to the hypervisor, found the target VM,
            and guestOperationsReady.
        """
        super().connect(host, user, pwd, port=port)
        if self.wait_for_guest:
            self.wait_until_guest_operations_ready()
        # And get our vm creds ready
        if not self.guest_auth:
            self.guest_auth = self.get_guest_auth()

        # GuestCmd use requests and get lots of SSL warnings
        requests.packages.urllib3.disable_warnings() # pylint: disable=no-member

        return self.conn

    def get_result(self, res):
        """
        Parses the result object into something nice.

        Also logs result, if logger is present.
        """
        # Default to error message
        summary = f"Unknown Result: {res}"

        if res and len(res) == 2:
            res = { 
                "return_code":res[0],
                "output":res[1]
            }
            if res["output"]:
                res["output"] = res["output"].decode()
            summary = f"""
--------------------------------------------------------------------------------
Return code : {res["return_code"]}
--------------------------------------------------------------------------------
{res["output"]} 
--------------------------------------------------------------------------------
"""
        self.log_msg(summary, "DEBUG")
        return res

    def get_guest_auth(self):
        return vim.vm.guest.NamePasswordAuthentication(
            username=self.vm_info["vm_user"],
            password=self.vm_info["vm_password"]
        )

    def delete_file_in_guest(self, file_path):
        content = self.conn.RetrieveContent()
        content.guestOperationsManager.fileManager.DeleteFileInGuest(
            self.vm,
            self.guest_auth,
            file_path
        )

    def retry(self, *args, **kwargs):
        if self.wait_for_ip:
            self.wait_until_guest_ip()
        return super().retry(*args, **kwargs)
