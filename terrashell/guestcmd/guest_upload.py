import requests

from pyVmomi import vim     # pylint: disable=no-name-in-module

from terrashell.guestcmd import GuestCmd

class GuestUpload(GuestCmd):
    """
    Upload a file to the GuestOS.

    Inspired by:
        https://github.com/vmware/vsphere-guest-run/blob/master/vsphere_guest_run/vsphere.py
        https://github.com/vmware/pyvmomi-community-samples/blob/master/samples/upload_file_to_vm.py
    """
    def __init__(self, local_path=None, file_contents=None, remote_path=None, overwrite=True, permissions=0o500, **kwargs):
        super().__init__(**kwargs)
        self.file_contents = file_contents
        self.local_path = local_path
        self.remote_path = remote_path
        self.overwrite = overwrite
        self.permissions = permissions
        if not self.remote_path:
            raise ValueError("remote_path is required!")

        self.file_contents = self.get_contents(local_path, file_contents)

    def get_file_attributes(self):
        family = self.vm_info.get("os_family", "Linux")
        if family == "Windows":
            self.log_msg(f"Using default permissions", "DEBUG")
            return vim.vm.guest.FileManager.FileAttributes()
        else:
            self.log_msg(f"Applying permission: {self.permissions}", "DEBUG")
            return vim.vm.guest.FileManager.PosixFileAttributes(
                permissions=self.permissions
            )

    def upload(self):
        content = self.conn.RetrieveContent()
        file_attribute = self.get_file_attributes()
        file_manager = content.guestOperationsManager.fileManager

        num_bytes = len(self.file_contents)
        self.log_msg(f"Uploading to {self.remote_path}", "DEBUG")

        url = file_manager.InitiateFileTransferToGuest(
            self.vm,
            self.guest_auth,
            self.remote_path,
            file_attribute,
            num_bytes,
            self.overwrite
        )
        res = requests.put(url, data=self.file_contents, verify=False)
        if res and res.ok:
            self.log_msg(
                f"Uploaded {num_bytes} bytes to {self.remote_path}",
                "DEBUG"
            )
        else:
            raise Exception(
                f"Failed to upload file to {self.remote_path}: {res}",
                "ERROR"
            )

        return res


    def run(self):
        return self.retry(self.upload)
