import requests

from terrashell.guestcmd import GuestCmd

class GuestDownload(GuestCmd):
    """
    Download a file from the GuestOS.

    Inspired by:
        https://github.com/vmware/vsphere-guest-run/blob/master/vsphere_guest_run/vsphere.py
    """
    def __init__(self, remote_path, **kwargs):
        super().__init__(**kwargs)
        self.remote_path = remote_path

    def download(self):
        content = self.conn.RetrieveContent()
        file_manager = content.guestOperationsManager.fileManager

        self.log_msg(f"Downloading {self.remote_path}...", "DEBUG")
        info = file_manager.InitiateFileTransferFromGuest(
            self.vm,
            self.guest_auth,
            self.remote_path
        )
        res = requests.get(info.url, verify=False)
        if res and res.ok:
            res = res.content
            self.log_msg(f"Downloaded {len(res)} bytes", "DEBUG")
        else:
            raise Exception(
                f"Failed to download file from {self.remote_path}: {res}",
                "ERROR"
            )

        return res

    def run(self):
        return self.retry(self.download)
