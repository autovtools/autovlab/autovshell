import uuid
import shlex

from gevent import sleep, GreenletExit 

from terrashell.guestcmd import GuestCmd
from .guest_download import GuestDownload

from pyVmomi import vim     # pylint: disable=no-name-in-module

class GuestExec(GuestCmd):
    """
    Executed a command instead the GuestOS.

    Inspired by:
        https://github.com/vmware/vsphere-guest-run/blob/master/vsphere_guest_run/vsphere.py
        https://github.com/vmware/pyvmomi-community-samples/blob/master/samples/execute_program_in_vm.py
    """
    def __init__(self, cmd, wait_for_completion=True, capture_output=True, **kwargs):
        super().__init__(**kwargs)
        self.kwargs = kwargs

        self.cmd = cmd
        self.wait_for_completion = wait_for_completion
        self.capture_output = capture_output
        self.program_spec = None

        self.downloader   = None
        self.proc_manager = None
        self.pid          = None

        defaults = {
            "redirect"  : "2>&1 >",
            "shell_cmd" : "/bin/sh -c",
            "tmp_dir"   : "/tmp/",
            "quote"     : True
        }
        for key, val in defaults.items():
            if not key in self.vm_info:
                self.vm_info[key] = val

        if self.capture_output:
            self.log = "{}{}.tmp".format(
                self.vm_info["tmp_dir"],
                uuid.uuid4()
            )
       
    def connect(self, *args, **kwargs):
        conn = super().connect(*args, **kwargs)
        if conn:
            for key in ["conn", "vm_info", "vm", "guest_auth"]:
                self.kwargs[key] = getattr(self, key)

            # We are connected, create our helpers
            self.log_msg("[HELPER] GuestDownload", "DEBUG")
            self.downloader = GuestDownload(
                remote_path=self.log,
                **self.kwargs
            )
            # Make sure our helpers are initialized
            self.downloader.connect(*args, **kwargs)
        return conn

    def get_program_spec(self):
        cmd = self.cmd
            
        if cmd and self.vm_info["quote"]:
            cmd = shlex.quote(cmd)

        tokens = shlex.split(self.vm_info["shell_cmd"])
        prog = tokens[0]
        args = tokens[1:]
        if cmd:
            args.append(cmd)
        if self.capture_output:
            args.append(self.vm_info["redirect"])
            args.append(self.log)
        args = " ".join(args)

        self.log_msg(f"{prog} {args}", "DEBUG") 

        ps = vim.vm.guest.ProcessManager.ProgramSpec(
            programPath=prog,
            arguments=args
        )
        return ps

    def get_proc(self):
        processes = self.proc_manager.ListProcessesInGuest(
            self.vm,
            self.guest_auth,
            [self.pid]
        )
        if not processes:
            self.log_msg(f"Failed to get process {self.pid}", "WARNING")
            return None
        return processes[0]

    def wait_for_result(self):
        res = None
        while res is None:
            self.log_msg(f"Waiting for process {self.pid} to finish...", "DEBUG")
            sleep(self.sleep_duration)
            proc = self.get_proc()
            res = self.check_result(proc)

        return res

    def check_result(self, proc):
        if proc is None or proc.exitCode is None:
            return None
        # Get results
        res = proc.exitCode
        self.log_msg(f"{self.pid} exited with code {proc.exitCode}", "DEBUG")
        if self.capture_output:
            res = (proc.exitCode, self.download_log())
            try:
                self.log_msg(f"Deleting {self.log}", "DEBUG")
                self.delete_file_in_guest(self.log)

            except GreenletExit as e:
                raise e
            except Exception as e:
                self.log_msg(f"Exception deleting {self.log}: {e}", "ERROR")

        return res

    def download_log(self):
        output = None
        try:
            self.log_msg(f"Downloading output from {self.log}", "DEBUG")
            output = self.downloader.run()
        except GreenletExit as e:
            raise e
        except Exception as e:
            self.log_msg("Error downloading output:", "ERROR")
            self.log_msg(e, "ERROR")
        return output

    def guest_exec(self):
        content = self.conn.RetrieveContent()
        self.proc_manager = content.guestOperationsManager.processManager
        self.program_spec = self.get_program_spec()
        
        self.pid = self.proc_manager.StartProgramInGuest(
            self.vm,
            self.guest_auth,
            self.program_spec
        ) 
        self.log_msg(f"Started guest process {self.pid}", "DEBUG")
        
        if not self.wait_for_completion:
            self.log_msg(f"Not waiting for completion", "DEBUG")
            return self.pid
        else:
            return self.wait_for_result()

    def run(self):
        return self.retry(self.guest_exec)
