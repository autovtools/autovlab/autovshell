import uuid
from gevent import GreenletExit
from terrashell.guestcmd import GuestCmd

from .guest_upload import GuestUpload
from .guest_exec import GuestExec

class GuestRunScript(GuestCmd):
    """
    Uploads, executes, and deletes a script in the GuestOS.

    Inspired by:
        https://github.com/vmware/vsphere-guest-run/blob/master/vsphere_guest_run/vsphere.py
    """
    def __init__(self, local_path=None, script_contents=None, remote_path=None, **kwargs):
        super().__init__(**kwargs)
        self.kwargs = kwargs
        self.remote_path = remote_path

        self.script_contents = self.get_contents(local_path, script_contents)

        if not self.remote_path:
            tmp_dir = self.vm_info.get("tmp_dir", "/tmp/")
            self.remote_path = f"{tmp_dir}{uuid.uuid4()}.tmp"
        

    def connect(self, *args, **kwargs):
        conn = super().connect(*args, **kwargs)
        if conn:
            # We are connected, create our helpers
            for key in ["conn", "vm_info", "vm", "guest_auth"]:
                self.kwargs[key] = getattr(self, key)

            self.log_msg("[HELPER] GuestUpload", "DEBUG")
            self.uploader = GuestUpload(
                file_contents=self.script_contents,
                remote_path=self.remote_path,
                permissions=0o500,
                **self.kwargs
            )
            # Initialize
            self.uploader.connect(*args, **kwargs)

            # Allow use of non shell scripts by
            #   setting the 'shell' to our payload
            self.kwargs["vm_info"]["shell_cmd"] = self.remote_path

            self.log_msg("[HELPER] GuestExec", "DEBUG")
            self.runner = GuestExec(
                cmd = None,
                wait_for_completion=True,
                capture_output=True,
                **self.kwargs
            )

            # Initialize
            self.runner.connect(*args, **kwargs)

            self.log_msg("[HELPERS] OK", "DEBUG")
            

        return conn

    def run_script(self):
        self.log_msg(f"[UPLOAD] {self.remote_path}", "DEBUG")
        self.uploader.run()
        self.log_msg("[UPLOAD] OK", "DEBUG")

        self.log_msg("[EXEC] START", "DEBUG")
        res = self.runner.run()
        self.log_msg("[EXEC] OK", "DEBUG")
        try:
            self.log_msg(f"[DELETE] {self.remote_path}", "DEBUG")
            self.delete_file_in_guest(self.remote_path)
            self.log_msg("[DELETE] OK", "DEBUG")

        except GreenletExit as e:
            raise e
        except Exception as e:
            self.log_msg(
                f"[DELETE] FAILED: {e}\n{self.remote_path} may still exist",
                "ERROR"
            )
        return self.get_result(res)

    def run(self):
        return self.retry(self.run_script)
