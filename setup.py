from setuptools import setup, find_packages
setup(
    name='terrashell',
    version='0.1',
    packages=find_packages(),
    install_requires=[
        'gevent',
        'coloredlogs',
        'pyvmomi',
        'pyvim',
        "vSphere-Automation-SDK @ git+https://github.com/vmware/vsphere-automation-sdk-python.git",
    ]
)
