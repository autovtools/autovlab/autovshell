# py-terrashell

A Python3 wrapper around `terrashell`.

# Purpose

Automation products can use `py-terrashell` to programmatically access `terrashell` / `PowerCLI` commands.

# Implemenation

Simply runs `terrashell` as a subprocess. This makes it easy to expose commands added to `terrashell` to `py-terrashell`,
but is not as efficient as a real re-implementing each function using https://github.com/vmware/pyvmomi/

# Setup
```bash
# Install terrashell
# Install python3 + pip dependencies
cd py-terrashell

# Uninstall + reinstall if you changed any code
pip3 uninstall -y terrashell
pip3 install .
```

